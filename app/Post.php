<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $primaryKey = 'id_post';

    protected $fillable = [
    	'title',
    	'content',
    	'user_id'
    ];
}
