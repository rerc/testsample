<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $primaryKey = 'id_comment';

    protected $fillable = [
    	'comment',
    	'post_id',
    	'user_id'
    ];
}
