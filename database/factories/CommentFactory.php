<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use App\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {

	$users = User::orderByRaw("RANDOM()")->first();
	$posts = Post::orderByRaw("RANDOM()")->first();

    return [
        'comment' => $faker->paragraph,
        'post_id' => $posts->id_post,
        'user_id' => $users->id_user
    ];
});
